import React from "react";
import ReactDOM from "react-dom/client";

// Import react router
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

// Declaration of router which contains an array of the 'routes' (path)
const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/product',
    element: <ProductList />
  },
  {
    path: '/cart',
    element: <Cart />
  }
])

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* Return RouterProvider */}
    <RouterProvider router={router}/>
  </React.StrictMode>
);
