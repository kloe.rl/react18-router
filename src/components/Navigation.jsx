// Import Link from react router
import { Link } from 'react-router-dom';

import "./Navigation.css";

function Navigation() {
  return (
    <ul className="Navigation">
        {/* Create links with Link to=""*/}
        <Link to="/">Home</Link>
        <Link to="/product">Product List</Link>
        <Link to="/cart">Cart</Link>
    </ul>
  );
}

export default Navigation;
